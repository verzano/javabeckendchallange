# Java Backend Challange #

This project was written using intelliJ IDEA and Maven.  As such loading the source code will be simplest in intelliJ IDEA.  This project uses Googles YouTube Java library for connecting to and querying their YouTube data API v3 service.

The unit tests in the project were written using junit.  To run these tests run the test step in maven.

In order to compile with the correct dependencies, Maven is required.  To create an executable jar you must build up to the package step, this will create a jar called javabackendchallange-1.0-jar-with-dependencies.jar.  This jar will contain the compiled Main class as well as all the dependencies required.  To run this executable jar supply it as the jar to the java -jar command and give it a single string argument.  The supplied argument will be the search term used when querying YouTube.

The main method will concurrently obtain information for the first 500 videos that match the given search term and will parse that string data.  It will finish by printing a histogram for the top ten most common words in each main sections for the results (Title, Description, and Tags).