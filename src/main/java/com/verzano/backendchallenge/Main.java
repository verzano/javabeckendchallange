package com.verzano.backendchallenge;

import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.VideoListResponse;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;

/**
 * Get any number (>50) of video information for the search term "duck"
 * Get any number (>50) of video information for the search term "pig"
 * Calculate the word frequency of appropriate terms in the responded data for each
 * Which words would you use to discriminate duck and pig videos and why?
 */
public class Main {
    /**
     * The number of videos to request from the YouTube 50 is current maximum for the API, so were just gonna grab that
     * many
     */
    private static final long NUMBER_OF_VIDEOS_RETURNED = 50;

    /**
     * the number of words for which to print out histograms.  This number is completely arbitrary
     */
    private static final int WORDS_TO_KEEP = 10;

    /**
     * The application key required by the API
     */
    private static final String KEY = "AIzaSyCenNO9NVZYb9c66aehuDHiENpnHnHypMk";

    /**
     * This volatile boolean is used to signify that all of the video ids have been retrieved from the YouTube API
     */
    private static volatile boolean allVideoIdsRetrieved = false;

    /**
     * This volatile boolean is used to signify that all of the videos have been retrieved from the YouTube API
     */
    private static volatile boolean allVideosRetrieved = false;

    /**
     * This {@link ExecutorService} is used to run threads for retrieving the videos from the YouTube API
     */
    private static final ExecutorService videoRetrievalService = Executors.newCachedThreadPool();

    /**
     * This {@link ExecutorService} is used to run threads for parsing the returned videos
     */
    private static final ExecutorService videoParsingService = Executors.newCachedThreadPool();

    /**
     * This @{link LinkedBlockingQueue} is used to store the all of the videoIds.  It's type is chosen such that it can
     * be accessed concurrently and will block when attempting to take when it is empty
     */
    private static final LinkedBlockingQueue<String> videoIds = new LinkedBlockingQueue<>();

    /**
     * This @{link LinkedBlockingQueue} is used to store the all of the videos.  It's type is chosen such that it can
     * be accessed concurrently and will block when attempting to take when it is empty
     */
    private static final LinkedBlockingQueue<VideoListResponse> videos = new LinkedBlockingQueue<>();

    /**
     * This concurrent map is used to record the number of video titles that contain each word found during parsing
     */
    private static final ConcurrentHashMap<String, Integer> titleWords = new ConcurrentHashMap<>();

    /**
     * This concurrent map is used to record the number of video descriptions containing each word found during parsing
     */
    private static final ConcurrentHashMap<String, Integer> descriptionWords = new ConcurrentHashMap<>();

    /**
     * This concurrent map is used to record the number of videos that contain each tag found during parsing
     */
    private static final ConcurrentHashMap<String, Integer> tags = new ConcurrentHashMap<>();

    /**
     * a human-ish readable name for the application to be sent to the YouTube API
     */
    private static final String APPLICATION_NAME = "javabackendchallanage";

    /**
     * These words would likely be very common to every description and would have little impact on what class of video
     * the description would be describing, therefor they are removed.  This could prove to be an
     */
    private static final HashSet<String> WORDS_TO_IGNORE = new HashSet<String>() {
        {
            add("the");
            add("and");
            add("to");
            add("in");
            add("a");
            add("for");
            add("of");
            add("this");
            add("with");
            add("is");
            add("on");
        }
    };

    /**
     * This main method is meant to pull data from the YouTube API in a concurrent manner.  The data that is pulled is
     * based on a provided search term.  Currently only the first argument supplied to the main method will be used to
     * query the YouTube API.  First a thread is started that pulls down the first 500 (this is the maximum you can pull
     * from the API) videos that match the provided search term, these will be done in chunks of 50 (50 is the maximum
     * size for a "page" of results).  While these are retrieved a thread for obtaining the video that these ids relates
     * to is started.  This thread uses an executor service to pull ids from the video ids queue as they are added and
     * spawn a new thread to actually obtain the video.  A third thread is started that is used for parsing the text
     * in the videos.  This thread also uses an executor service to take videos from the video queue, and increment the
     * count of words in their text components to the appropriate maps.  Once that final video parsing thread completes
     * histograms for the top 10 words (chosen to make the output shorter, all of the words should likely be used in a
     * full version of application) are printed to the screen with their associated counts.
     */
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        if(args.length != 1) {
            System.err.println("No search terms specified as arguments to this program, exiting...");
            System.exit(0);
        }

        System.out.println("Obtaining video information for " + args[0] + "...");

        new Thread(() -> {
            System.out.println("Starting video id retrieval");
            getVideoIdsForSearchTerm(args[0]);
            System.out.println("Video id retrieval complete");
        }).start();

        final long[] getVideoRunTime = new long[]{0L};
        new Thread(() -> {
            System.out.println("Starting video retrieval");
            while(!videoIds.isEmpty() || !allVideoIdsRetrieved) {
                try {
                    final String id = videoIds.take();
                    videoRetrievalService.submit(() -> {
                        long getVideoStart = System.currentTimeMillis();
                        getVideoForId(id);
                        synchronized (getVideoRunTime) {
                            getVideoRunTime[0] += System.currentTimeMillis() - getVideoStart;
                        }
                    });
                } catch (InterruptedException e) {
                    System.err.println("Interrupted while trying to take from the video ids");
                    e.printStackTrace();
                }
            }
            allVideosRetrieved = true;
            System.out.println("Video retrieval complete");

            try {
                videoRetrievalService.shutdown();
                videoRetrievalService.awaitTermination(60, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                System.err.println("Interrupted while awaiting video retrieval service terminations");
                e.printStackTrace();
            } finally {
                videoRetrievalService.shutdownNow();
            }
        }).start();

        final long[] parseVideoRunTime = new long[]{0};
        Thread videoListResponseThread = new Thread(() -> {
            System.out.println("Starting video parsing");
            while(!videos.isEmpty() || !allVideosRetrieved) {
                try {
                    final VideoListResponse response = videos.take();
                    videoParsingService.submit(() -> {
                        long videoParsingStart = System.currentTimeMillis();
                        parseVideo(response);
                        synchronized (parseVideoRunTime) {
                            parseVideoRunTime[0] += System.currentTimeMillis() - videoParsingStart;
                        }
                    });
                } catch (InterruptedException e) {
                    System.err.println("Interrupted while trying to take from the video list responses");
                    e.printStackTrace();
                }
            }
            System.out.println("Video parsing complete");

            try {
                videoParsingService.shutdown();
                videoParsingService.awaitTermination(60, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                System.err.println("Interrupted while awaiting video parsing service terminations");
                e.printStackTrace();
            } finally {
                videoParsingService.shutdownNow();
            }
        });

        videoListResponseThread.start();
        try {
            videoListResponseThread.join();
        } catch (InterruptedException e) {
            System.err.println("Interrupted while waiting for video list response thread to join");
            e.printStackTrace();
        }


        System.out.println("----- Title Words -----");
        printHistogram(titleWords);

        System.out.println("----- Description Words -----");
        printHistogram(descriptionWords);

        System.out.println("----- Tags -----");
        printHistogram(tags);

        System.out.println("----- Stats -----");
        System.out.println("Total run time: " + (System.currentTimeMillis() - startTime) + " milliseconds");
        System.out.println("getVideo() total run time: " + getVideoRunTime[0]);
        System.out.println("parseVideo() total run time: " + parseVideoRunTime[0]);

    }

    /**
     * This method will query the YouTube API for all of the ids that match the the given {@link String}.  The YouTube
     * API will return these results in "pages" that have a maximum of 50 results per page.  Once this method has
     * removed all of the ids from a page it will get the "nextPageId" from the page and repeat the process until that
     * "nextPageId" is null, signifying that no more pages are available.  This happens when no more videos match the
     * supplied {@link String} or when you have retrieved 500 results (the max retrievable from the YouTube API).
     *
     * All of these video ids are added to the {@link #videoIds} queue.
     *
     * @param searchTerm The {@link String} for which to query the YouTube data API
     */
    private static void getVideoIdsForSearchTerm(String searchTerm) {
        try {
            YouTube.Search.List search = new YouTube.Builder(new NetHttpTransport(), new JacksonFactory(), request -> {})
                    .setApplicationName(APPLICATION_NAME)
                    .build()
                    .search()
                    .list("id")
                    .setFields("items(id(videoId)),nextPageToken")
                    .setKey(KEY)
                    .setType("video")
                    .setQ(searchTerm)
                    .setMaxResults(NUMBER_OF_VIDEOS_RETURNED);

            String pageToken;
            do {
                final SearchListResponse response = search.execute();
                if (response.getItems() != null) {
                    response.getItems().forEach(searchResult -> videoIds.add(searchResult.getId().getVideoId()));
                }
                pageToken = response.getNextPageToken();
                search.setPageToken(pageToken);
            } while(pageToken != null);
        } catch (IOException e) {
            System.err.println("An error occurred while retrieving video ids");
            e.printStackTrace();
        } finally {
            allVideoIdsRetrieved = true;
        }
    }

    /**
     * This method queries the YouTube API for a {@link VideoListResponse} matching the supplied {@link String}.  Once
     * retrieved it will add it to the {@link #videos} queue.
     *
     * @param id The {@link String} representing the {@link VideoListResponse} for which to query the YouTube API.
     */
    private static void getVideoForId(String id) {
        try {
            videos.add(new YouTube.Builder(new NetHttpTransport(), new JacksonFactory(), request -> {})
                    .setApplicationName(APPLICATION_NAME)
                    .build()
                    .videos()
                    .list("snippet")
                    .setFields("items(snippet(title,description,tags))")
                    .setKey(KEY)
                    .setId(id)
                    .execute());
        } catch (IOException e) {
            System.err.println("Something went wrong while querying the YouTube API for a video with id " + id);
            e.printStackTrace();
        }
    }

    /**
     * This method will parse through a {@link VideoListResponse}'s internal videos' (which should be only one video)
     * title, description, and tags if they are present.  It will pull out the title and description words as well as
     * the tags and increment the count of those words in their respective maps by 1 (or set them to 1 if they did not
     * yet exist in the maps).  Multiple occurrences of a word in one of the videos text components will only be counted
     * once or each individual text component.  The parse words {@link #parseWords(Map, String[])} call will parse
     * combine words regardless of capitalization and will remove internal symbols from words, i.e. (red & RE^d are
     * considered to be the same word).  A full version of this application would likely have more intelligent logic
     * for determining that misspelled words are the same (either by incorrect capitalization or incorrect symbols).
     *
     * @param response A {@link VideoListResponse} to parse the title, description, and tags of
     */
    private static void parseVideo(VideoListResponse response) {
        response.getItems().forEach(video -> {
            parseWords(titleWords, video.getSnippet().getTitle().split("\\s+"));
            parseWords(descriptionWords, video.getSnippet().getDescription().split("\\s+"));
            List<String> videoTags = video.getSnippet().getTags();
            if(videoTags != null) {
                parseWords(tags, videoTags.toArray(new String[videoTags.size()]));
            }
        });
    }

    /**
     * This method looks through every String in an array and increments the count of that word in the supplied
     * {@link Map}.  Before words are counted they first are made completely lower cased and have all symbols removed.
     * If the seenWords {@link HashSet} contains that word then that word is skipped and the for loop continues.
     * Otherwise if the map already contains the word its count is incremented (or set to 1 if the map does not
     * contain it as a key).
     *
     * To do this in a more functional fashion this method could return a new version of the supplied map
     * that has its values incremented, and leave the supplied map unmodified.
     *
     * @param wordsMap A {@link Map} for which to increment the count of a word.  The key is a {@link String}
     *                 representing a word from the {@link VideoListResponse}, the value is the count of the
     *                 {@link VideoListResponse}s that contain that word.  This {@link Map} should be passed
     *                 continuously to this method so that the words in the {@link VideoListResponse}s can be counted
     *                 properly.
     * @param words The list of words from the {@link VideoListResponse} to parse through
     */
    protected static void parseWords(Map<String, Integer> wordsMap, String[] words) {
        HashSet<String> seenWords = new HashSet<>();
        for(String word : words) {
            word = word.toLowerCase().replaceAll("[^\\p{L}\\p{Nd}]+", "");
            if(word.isEmpty() || WORDS_TO_IGNORE.contains(word) ||  seenWords.contains(word)) continue;

            Integer count = wordsMap.get(word);
            if(count == null) {
                count = 0;
            }
            wordsMap.put(word, count + 1);
            seenWords.add(word);
        }
    }

    /**
     * This method is used to print out a histogram for the N words with the highest frequency in the supplied
     * wordsMap where N is equal to {@link #WORDS_TO_KEEP}.
     *
     * @param wordsMap The {@link Map} containing the words for which to print the counts.
     */
    private synchronized static void printHistogram(Map<String, Integer> wordsMap) {
        List<Map.Entry<String, Integer>> wordToCountList = new ArrayList<>(wordsMap.entrySet());
        Collections.sort(wordToCountList, (wtc1, wtc2) -> Integer.compare(wtc2.getValue(), wtc1.getValue()));

        List<Map.Entry<String, Integer>> subList = wordToCountList.subList(0, WORDS_TO_KEEP);
        int maxLength = 0;
        for(Map.Entry<String, Integer> wTc : subList) {
            if(wTc.getValue() == 1) continue;
            maxLength = Math.max(maxLength, wTc.getKey().length());
        }
        for(Map.Entry<String, Integer> wtc : subList) {
            if(wtc.getValue() == 1) continue;
            System.out.printf("%-" + (maxLength + 1) + "s %d:", wtc.getKey(), wtc.getValue());
            for (int i = 0; i < wtc.getValue(); i++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
