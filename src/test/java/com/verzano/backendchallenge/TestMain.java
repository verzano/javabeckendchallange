package com.verzano.backendchallenge;

import org.junit.Test;

import java.util.TreeMap;

public class TestMain {
    /**
     * This test ensures that the parseWords method views words as identical even if their are capitalized differently
     */
    @Test
    public void parseWordsCapitalizationTest() {
        String[][] words = new String[][] {
                {"red"},
                {"RED"},
                {"ReD"}
        };

        TreeMap<String, Integer> wordWithCounts = new TreeMap<>();
        for (String[] wordArray : words) {
            Main.parseWords(wordWithCounts, wordArray);
        }

        // Assert that both red and RED were viewed the same
        assert wordWithCounts.size() == 1;

        // Assert that the only word is the lowercase version, 'red'
        assert wordWithCounts.firstEntry().getKey().equals("red");
    }

    /**
     * The purpose of this test is to assert that the parseWords method will group together words properly, if in a
     * single array a word is repeated it should only be counted once
     */
    @Test
    public void parseWordsCountTest() {
        String[][] words = new String[][] {
                {"red", "red", "red"},
                {"red", "red"},
                {"red"}
        };

        TreeMap<String, Integer> wordToCountMap = new TreeMap<>();
        for (String[] wordArray : words) {
            Main.parseWords(wordToCountMap, wordArray);
        }

        // Should have exactly 3 entries
        assert wordToCountMap.firstEntry().getValue() == 3;
    }

    /**
     * The purpose of this test is to ensure that the parseWords method will properly remove any symbols within a word
     * when parsing it
     */
    @Test
    public void parseWordsSymbolRemoval() {
        String[][] words = new String[][] {
                {"r!@#$%^&*()_+-=ed[]{}\\|./>?\";:,<`~"}
        };

        TreeMap<String, Integer> wordWithCounts = new TreeMap<>();
        for (String[] wordArray : words) {
            Main.parseWords(wordWithCounts, wordArray);
        }

        // Should have only a single word, which is red
        assert wordWithCounts.size() == 1;
        assert wordWithCounts.firstEntry().getKey().equals("red");
    }
}
